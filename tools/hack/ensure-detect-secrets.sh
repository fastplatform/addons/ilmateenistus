#!/bin/bash


set -o errexit
set -o nounset
set -o pipefail

MINIMUM_DETECT_SECRETS_VERSION=0.14.2

check_detect_secrets_version() {

  if ! [ -x "$(command -v detect-secrets)" ]; then
    echo "detect-secrets not in path !"
    echo "it can be installed with python-pip: pip install detect-secrets"
    return 1
  fi

  current_detect_secrets_version=$(detect-secrets --version)
  if [[ "${MINIMUM_DETECT_SECRETS_VERSION}" != $(echo -e "${MINIMUM_DETECT_SECRETS_VERSION}\n${current_detect_secrets_version}" | sort -s -t. -k 1,1 -k 2,2n -k 3,3n | head -n1) ]]; then
    cat <<EOF
Current detect-secrets version: ${current_detect_secrets_version}.
Requires ${MINIMUM_DETECT_SECRETS_VERSION} or greater.
EOF
    return 2
  fi
}

check_detect_secrets_version
