#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

ILMATEENISTUS_VERSION=${ILMATEENISTUS_VERSION:=$(grep 'ILMATEENISTUS_VERSION\s*=' VERSION | awk '{print $3}')}
ILMATEENISTUS_CI_VERSION=${ILMATEENISTUS_CI_VERSION:=$(grep 'ILMATEENISTUS_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [[ -z "${CI}" ]]; then
    VERSION=${ILMATEENISTUS_VERSION}
  else
    VERSION="${ILMATEENISTUS_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_ILMATEENISTUS_VERSION ${VERSION}"

ILMATEENISTUS_TAG=${VERSION/+/-}
echo "STABLE_ILMATEENISTUS_TAG ${ILMATEENISTUS_TAG}"
echo "STABLE_ILMATEENISTUS_TAG_PREFIXED_WITH_COLON :${ILMATEENISTUS_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="pwcfasteu.azurecr.io"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/addons/ilmateenistus/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"
