load("@rules_pkg//:pkg.bzl", "pkg_tar")

def release(name, kustomize_resources, image_manifest, graphql_queries, visibility=None):
    """Instantiate rules to manage the release of Kustomize/Kubernetes manifests.

       Instantiated rules: 
        - :name_kustomize_manifest to build a Kustomize manifest archive
        - :name_deploy_manifest to build a ready-to-deploy Kubernetes manifest
       Kubernetes namespace can be defined via the following Bazel build argument:
        - --define namespace=target-namespace)
    """
    pkg_tar(
        name = name + "_kustomize_manifest",
        extension = "tar.gz",
        strip_prefix = "/",
        srcs = [
            ":kustomization.yaml",
            kustomize_resources,
        ],
        remap_paths = {
            "": "addons/ilmateenistus/",
        },
        visibility = visibility,
    )

    native.genrule(
        name = name + "_deploy_manifest",
        srcs = [
            ":kustomization.yaml",
            kustomize_resources,
        ],
        outs = [
            name + "-deploy-manifest.yaml",
        ],
        cmd = " && ".join([
            "KUSTOMIZE_RESOURCES_SRCS=$$(echo $(locations %s))" % kustomize_resources,
            "echo $$KUSTOMIZE_RESOURCES_SRCS | xargs -n 1 sh -c 'mkdir -p $(@D)/out/$$(dirname $$0)'",
            "echo $$KUSTOMIZE_RESOURCES_SRCS | xargs -n 1 sh -c 'cp -L $$0 $(@D)/out/$$0'", # this to remove symlink
            "cp $(location :kustomization.yaml) $(@D)/out",
            "export HOME=~/", # workaround to source the ~/.gitconfig file of the host platform
            "$(location //tools:kustomize) build $(@D)/out > $@",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = visibility
    )

    native.genrule(
        name = name + "_kustomize_manifest_config",
        srcs = [
            image_manifest,
            kustomize_resources,
        ],
        outs = [
            "kustomization.yaml",
        ],
        cmd = " && ".join([ 
            "$(location //tools:kustomize) create",
            "echo resources: >> kustomization.yaml",
            "echo - manifests >> kustomization.yaml",
            "cat $(location %s) | xargs -n 2 sh -c '$(location //tools:kustomize) edit set image $${0#*/}=$$0:$$1'" % image_manifest,
            "mv kustomization.yaml $(location :kustomization.yaml)",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = [
            "//visibility:private",
        ],
    )

    native.genrule(
        name = name + "_graphql_query_collections",
        srcs = [
            graphql_queries,
        ],
        outs = [
            "query_collections.yaml",
        ],
        cmd = "|".join([
            "$(location //tools:yq) e -n '[{\"name\":\"allowed-queries\", \"definition\": {\"queries\": []}}]' > $@",
            "echo $(locations %s)" % graphql_queries,
            "xargs -n 1 sh -c 'echo $$(cat $$0 | sed 's/#.*//' | tr -s \\[:space:\\] \" \")'",
            "sed -E 's/^(mutation|query|subscription)/\\1/g'",
            "(grep -E '^(mutation|query|subscription)' || true)",
            "sed 's/\"/\\\\\\\\\"/g'",
            "xargs -d '\n' -I % printf '{\"name\":\"\",\"query\":\"%\"}\n'",
            "xargs -d '\n' -I % sh -c \"q='%' $(location //tools:yq) e -i '.[0].definition.queries += env(q)' $@\";",
        ]) + ";".join([
            "nb_queries=$$($(location //tools:yq) e '.[0].definition.queries | length' $@)",
            "if (( $$nb_queries > 0 )); then for i in $$(seq 0 $$($(location //tools:yq) e '.[0].definition.queries | length' $@)); do $(location //tools:yq) e -i \".[0].definition.queries.[$$((i-1))].name = \\\"addons-ilmateenistus-$$(printf '%03d' $$i)\\\"\" $@; done; fi",
            "$(location //tools:yq) e -i '... style=\"\" | .[0].definition.queries[].query style=\"literal\"' $@",
        ]),
        tools = [
            "//tools:yq",
        ],
        visibility = [
            "//visibility:private",
        ],
    )
