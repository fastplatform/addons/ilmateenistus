# meteorology/weather

```meteorology/weather``` is a [FastAPI](https://fastapi.tiangolo.com/) (+ [Graphene](https://graphene-python.org/) framework) service that provides climatic and weather observations and forecasts, and is based on public data exposed by [Ilmateenistus](https://www.ilmateenistus.ee/) (Estonian Weather Agency).

It exposes 3 types of objects through a single GraphQL endpoint:
- **weather observations (past data)**: fetched from the [Ilmateenistus observations XML endpoint](https://www.ilmateenistus.ee/teenused/ilmainfo/eesti-vaatlusandmed-xml/)
- **weather forecasts**: fetched from the [Ilmateenistus forecast XML endpoint](https://www.ilmateenistus.ee/teenused/ilmainfo/eesti-prognoos-xml/)
- **weather alerts**: fetched from the [Ilmateenistus warnings XML endpoint](https://www.ilmateenistus.ee/teenused/ilmainfo/eesti-prognoos-xml/)

This service therefore exposes a GraphQL ontology that can be natively federated at a higher level into another GraphQL ontology. It instantly enables the modular aspect of Ilmateenistus's custom logic. The exposed GraphQL schema is compliant with the [FaST Weather GraphQL ontology](https://gitlab.com/fastplatform/pypi/fastplatform/-/blob/master/fastplatform-graphql/fastplatform_graphql/types/weather.py).

The service embarks the [list of all Estonian cities and counties with their coordinates](data/places.csv), which is loaded in memory and geographically indexed when the service starts. This geographical index is then used to map location names (as returned by the Ilmateenistus service) to geographical locations (latitude, longitude).

## Dependencies

This service depends on the availability of the XML endpoints exposed by Ilmateenistus, mentioned above.
It also depends on the geographical index of [Estonian locations used to map names to coordinates](data/places.csv): this file was purpose built for FaST from various public datasets from the [Estonian Land Board](https://www.maaamet.ee/).

## Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)

## Environment variables

- `ILMATEENISTUS_XML_OBSERVATIONS_URL`: URL of Ilmateenistus observations XML endpoint (defaults to `https://www.ilmateenistus.ee/ilma_andmed/xml/observations.php`)
- `ILMATEENISTUS_XML_OBSERVATIONS_MAX_AGE`: caching duration of the observations XML response (defaults to `60` seconds)
- `ILMATEENISTUS_XML_FORECASTS_URL`: URL of Ilmateenistus forecasts XML endpoint (defaults to `https://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php`)
- `ILMATEENISTUS_XML_FORECASTS_MAX_AGE`: caching duration of the forecasts XML response (defaults to `60` seconds)
- `ILMATEENISTUS_XML_ALERTS_URL`: URL of Ilmateenistus alerts XML endpoint (defaults to `https://www.ilmateenistus.ee/ilma_andmed/xml/hoiatus.php`)
- `ILMATEENISTUS_XML_ALERTS_MAX_AGE`: caching duration of the alerts XML response (defaults to `60` seconds)

## Development Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686](http://localhost:16686)

Start the service:
```
make start
```

The API server is now started and available at [http://localhost:7777/graphql](http://localhost:7777/graphql), with live reload enabled.

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run
```

## Sample 

A sample (full) GraphQL query is included in the [tests](tests) folder.
