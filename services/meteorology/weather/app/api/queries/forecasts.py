import json

import graphene
from graphql import GraphQLError
from shapely.geometry import shape

from fastplatform_graphql.types.weather import Sample, IntervalType

from app.lib.gis import Projection
from app.lib import ilmateenistus


class Forecasts:

    # Describe capabilities
    forecasts_implemented = graphene.Boolean(default_value=True)
    forecasts_interval_types = graphene.List(IntervalType)

    def resolve_forecasts_interval_types(self, info):
        return [IntervalType.day]

    # Node and resolver for forecasts
    forecasts = graphene.List(
        Sample,
        geometry=graphene.Argument(graphene.String, required=True),
        interval_type=graphene.Argument(IntervalType, required=True),
    )

    def resolve_forecasts(self, info, geometry, interval_type):
        """Resolver for `forecasts` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string on ETRS89 datum

        Returns:
            list -- List of weather Samples
        """
        if not info.context["request"].headers["X-Hasura-User-Id"]:
            return None

        if interval_type not in self.resolve_forecasts_interval_types(info):
            return None

        geometry = shape(json.loads(geometry))
        geometry = Projection.etrs89_to_estonian_1997(geometry)
        centroid = geometry.centroid
        samples = ilmateenistus.ilmateenistus_client.get_forecasts_for_xy(centroid.x, centroid.y)

        return [Sample(**s) for s in samples]