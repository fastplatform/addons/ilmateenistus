import json

import graphene
from graphql import GraphQLError
from shapely.geometry import shape

from fastplatform_graphql.types.weather import Sample, IntervalType

from app.lib.gis import Projection
from app.lib import ilmateenistus


class Observations:

    # Describe capabilities
    observations_implemented = graphene.Boolean(default_value=True)
    observations_interval_types = graphene.List(IntervalType)

    def resolve_observations_interval_types(self, info):
        return [IntervalType.hour]

    # Node and resolver for observations
    observations = graphene.List(
        Sample,
        geometry=graphene.Argument(graphene.String, required=True),
        interval_type=graphene.Argument(IntervalType, required=True),
        date_from=graphene.Argument(graphene.Date, required=False),
        date_to=graphene.Argument(graphene.Date, required=False),
    )

    def resolve_observations(self, info, geometry, interval_type, date_from, date_to):
        """Resolver for `observations` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string
            date_from {date} -- [description]
            date_to {str} -- [description]

        Returns:
            list -- List of weather Samples
        """
        if not info.context["request"].headers["X-Hasura-User-Id"]:
            return None

        geometry = shape(json.loads(geometry))
        geometry = Projection.etrs89_to_estonian_1997(geometry)
        centroid = geometry.centroid
        samples = ilmateenistus.ilmateenistus_client.get_observations_for_xy(centroid.x, centroid.y)

        return [Sample(**s) for s in samples]


