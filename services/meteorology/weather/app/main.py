import os
import logging

from fastapi import FastAPI
from fastapi.middleware import Middleware

import graphene
from starlette_graphene3 import GraphQLApp

from app.settings import config
from app.tracing import Tracing

from app.api.query import Query

# FastAPI
app = FastAPI()

# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(
        schema=graphene.Schema(query=Query, auto_camelcase=False)
    ),
)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    logger.debug(config)


@app.on_event("shutdown")
async def shutdown():
    pass
