from datetime import datetime, timedelta
import csv
import os
import logging
import pytz
import requests
import xmltodict
import json

import dateutil.parser
from opentelemetry import trace
from shapely.geometry import shape, Point
from shapely.ops import transform

from app.settings import config
from app.lib.gis import Projection, add_crs

TZ = pytz.utc

# Logging
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class IlmateenistusClient:
    def __init__(self):
        # Mapping from Ilmateenistus weather codes/icons to FaST codes/icons
        self.icons_mapping = {}
        with open(config.API_DIR / "data/ilmateenistus-codes.csv", "r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.icons_mapping[row["ILMATEENISTUS_DESC_EN"]] = row["FAST_CODE"]

        # Mapping from NSWE to degrees
        self.bearing_mapping = {
            "North wind": 0,
            "Northeast wind": 45,
            "East wind": 90,
            "Southeast wind": 135,
            "South wind": 180,
            "Southwest wind": 225,
            "West wind": 270,
            "Northwest wind": 315,
        }

        # Places coordinates as these are not returned by the XML service
        # (only the place name, which is often ambiguous)
        self.places = {}
        with open(config.API_DIR / "data/places.csv", "r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                # In the csv file, the places are sorted in decreasing order of administrative
                # importance. If there are multiple matches for the same key, we only use the
                # the first match (the most important from an administrative point of view)
                if row["KEY"] not in self.places:
                    self.places[row["KEY"]] = row

        self.observations_cache = {
            "data": None,
            "timestamp": datetime.min,
        }
        self.forecasts_cache = {
            "data": None,
            "timestamp": datetime.min,
        }
        self.alerts_cache = {"data": None, "timestamp": datetime.min}

    def fetch_forecasts(self):
        """Fetch forecast for all of Estonia and store it in memory
        as a dict
        """
        response = requests.get(config.ILMATEENISTUS_XML_FORECASTS_URL)
        data = xmltodict.parse(response.content)
        data = data.get("forecasts", None)
        if data is None:
            data = []
        else:
            data = data.get("forecast", [])

        forecasts = self.parse_forecasts(data)

        self.forecasts_cache = {"data": forecasts, "timestamp": datetime.now()}

    def fetch_observations(self):
        """Fetch observations for all of Estonia and store it in memory
        as a dict
        """
        response = requests.get(config.ILMATEENISTUS_XML_OBSERVATIONS_URL)
        data = xmltodict.parse(response.content)
        observations = data.get("observations", [])
        if observations is None:
            observations = []

        for station in observations["station"]:
            point = Point((float(station["longitude"]), float(station["latitude"])))
            station["point"] = Projection.wgs84_to_estonian_1997(point)

        self.observations_cache = {"data": observations, "timestamp": datetime.now()}

    def fetch_alerts(self):
        """Fetch alerts for all of Estonia and store it in memory
        as a dict
        """
        response = requests.get(config.ILMATEENISTUS_XML_ALERTS_URL)
        data = xmltodict.parse(response.content)
        warnings = data.get("warnings", [])
        if warnings is None:
            warnings = []
        else:
            warnings = warnings.get("warning", [])
            if warnings is None:
                warnings = []

        self.alerts_cache = {"data": warnings, "timestamp": datetime.now()}

    def parse_forecasts(self, data):

        forecasts = {}

        for item in data:

            date = datetime.strptime(item["@date"], "%Y-%m-%d")
            if date not in forecasts:
                forecasts[date] = {}

            for period in ["day", "night"]:

                if period not in item:
                    continue

                if "place" not in item[period]:
                    continue

                # Load the wind values into a temp object
                winds = {}

                for wind in item[period].get("wind", []):
                    location_key = wind.get("name", "").upper()
                    if location_key not in self.places:
                        logger.warning(
                            f'Could not find wind location "{location_key}" in the list of known places, ignoring it'
                        )
                        continue

                    direction = wind.get("direction", None)
                    if direction:
                        wind_bearing = self.bearing_mapping.get(direction, None)
                    else:
                        wind_bearing = None

                    winds[location_key] = {
                        "point": Point(
                            float(self.places[location_key]["X"]),
                            float(self.places[location_key]["Y"]),
                        ),
                        "wind_speed": wind.get("speedmin"),
                        "wind_speed_max": wind.get("speedmax"),
                        "wind_gust": wind.get("gust"),
                        "wind_bearing": wind_bearing,
                    }

                # For each place, extract the location
                for place in item[period]["place"]:

                    place_key = place["name"].upper()
                    if place_key not in self.places:
                        logger.warning(
                            f'Could not find place "{place_key}" in the list of known places, ignoring it'
                        )
                        continue

                    # Store coordinates
                    x = float(self.places[place_key]["X"])
                    y = float(self.places[place_key]["Y"])
                    point = Point(x, y)
                    if place_key not in forecasts[date]:
                        forecasts[date][place_key] = {
                            "key": place_key,
                            "computed_at": datetime.now(),
                            "point": point,
                        }

                    geometry = Projection.estonian_1997_to_etrs89(point)
                    geometry = add_crs(
                        geometry.__geo_interface__, config.EPSG_SRID_ETRS89
                    )

                    forecasts[date][place_key]["location"] = {
                        "id": place_key,
                        "name": place["name"],
                        "geometry": geometry,
                    }

                    # Store the icon for the day period (or if not present, for the night)
                    if "phenomenon" in place:
                        if period == "day" or (
                            period == "night"
                            and "icon" not in forecasts[date][place_key]
                        ):
                            forecasts[date][place_key]["icon"] = self.icons_mapping.get(
                                place["phenomenon"], "other"
                            )

                    # Store the icon for the day period (or if not present, for the night)
                    if "text" in place:
                        if period == "day" or (
                            period == "night"
                            and "text" not in forecasts[date][place_key]
                        ):
                            forecasts[date][place_key]["summary"] = place["text"]

                    if "tempmax" in place:
                        if (
                            "temperature_max" in forecasts[date][place_key]
                            and place["tempmax"]
                            > forecasts[date][place_key]["temperature_max"]
                        ) or ("temperature_max" not in forecasts[date][place_key]):
                            forecasts[date][place_key]["temperature_max"] = place[
                                "tempmax"
                            ]

                    if "tempmin" in place:
                        if (
                            "temperature_min" in forecasts[date][place_key]
                            and place["tempmin"]
                            < forecasts[date][place_key]["temperature_min"]
                        ) or ("temperature_min" not in forecasts[date][place_key]):
                            forecasts[date][place_key]["temperature_min"] = place[
                                "tempmin"
                            ]

                    # Use the temp values for whole Estonia if we do not have them for this specific place
                    if (
                        period == "night"
                        and "tempmax" in item[period]
                        and "temperature_max" not in forecasts[date][place_key]
                    ):
                        forecasts[date][place_key]["temperature_max"] = item[period][
                            "tempmax"
                        ]

                    if (
                        period == "night"
                        and "tempmin" in item[period]
                        and "temperature_min" not in forecasts[date][place_key]
                    ):
                        forecasts[date][place_key]["temperature_min"] = item[period][
                            "tempmin"
                        ]

                    # Find the closest wind location to this place
                    wind = min(
                        winds.values(),
                        key=lambda w: w["point"].distance(point),
                        default=None,
                    )
                    if wind is not None:
                        forecasts[date][place_key].update(
                            {
                                "wind_speed": wind["wind_speed"],
                                "wind_speed_max": wind["wind_speed_max"],
                                "wind_gust": wind["wind_gust"],
                                "wind_bearing": wind["wind_bearing"],
                            }
                        )

            # If we have no specific place for this date, then store the forecast for the whole
            # of Estonia
            if not forecasts[date].keys():
                temperature_min = min(
                    item.get("night", {}).get("tempmin", None),
                    item.get("day", {}).get("tempmin", None),
                )
                temperature_max = max(
                    item.get("night", {}).get("tempmax", None),
                    item.get("day", {}).get("tempmax", None),
                )
                text = item.get("day", item.get("night").get("text", None)).get(
                    "text", None
                )
                phenomenon = item.get(
                    "day", item.get("night").get("phenomenon", None)
                ).get("phenomenon", None)
                icon = self.icons_mapping.get(phenomenon, "other")
                point = Point(558932.92, 6495483.58)
                geometry = Projection.estonian_1997_to_etrs89(point)
                forecasts[date]["EE"] = {
                    "key": "EE",
                    "name": "Estonia",
                    "point": Point(558932.92, 6495483.58),
                    "location": add_crs(geometry.__geo_interface__, config.EPSG_SRID_ETRS89),
                    "temperature_min": temperature_min,
                    "temperature_max": temperature_max,
                    "summary": text,
                    "icon": icon,
                    "computed_at": datetime.now(),
                }

        return forecasts

    def get_forecasts_for_xy(self, x, y):
        """Retrieve the forecast for a point given in XY cordinates on EPSG:3301

        Arguments:
            x {float} -- Easting
            y {float} -- Northing

        Returns:
            [dict] -- [description]
        """
        if datetime.now() > self.forecasts_cache["timestamp"] + timedelta(
            seconds=int(config.ILMATEENISTUS_XML_FORECASTS_MAX_AGE)
        ):
            self.fetch_forecasts()
        forecasts = self.forecasts_cache["data"]

        samples = []

        origin = {
            "name": "Riigi Ilmateenistusele",
            "url": "www.ilmateenistus.ee",
            "language": "et-ee",
        }

        for valid_from, forecasts in forecasts.items():

            closest_forecast = min(
                forecasts.values(),
                key=lambda f: (x - f["point"].x) ** 2 + (y - f["point"].y) ** 2,
                default=None,
            )
            valid_to = valid_from + timedelta(hours=24)

            sample = {
                "id": f'{closest_forecast["key"]}|{valid_from.strftime("%Y-%m-%d")}|{closest_forecast["computed_at"]}',
                "origin": origin,
                "fetched_at": self.forecasts_cache["timestamp"],
                "valid_from": valid_from,
                "valid_to": valid_to,
                "sample_type": "forecast",
                "interval_type": "day",
                "location": closest_forecast["location"],
                "computed_at": closest_forecast["computed_at"],
                "icon": closest_forecast["icon"],
                "temperature_min": closest_forecast.get("temperature_min", None),
                "temperature_max": closest_forecast.get("temperature_max", None),
                "summary": closest_forecast.get("summary", None),
                "wind_speed": closest_forecast.get("wind_speed", None),
                "wind_speed_max": closest_forecast.get("wind_speed_max", None),
                "wind_gust": closest_forecast.get("wind_gust", None),
                "wind_bearing": closest_forecast.get("wind_bearing", None),
            }

            samples += [sample]

        return samples

    def get_observations_for_xy(self, x, y):
        """Retrieve the closest observations for a point given in XY cordinates on EPSG:3301

        Arguments:
            x {float} -- Easting
            y {float} -- Northing

        Returns:
            [type] -- [description]
        """
        if datetime.now() > self.observations_cache["timestamp"] + timedelta(
            seconds=int(config.ILMATEENISTUS_XML_OBSERVATIONS_MAX_AGE)
        ):
            self.fetch_observations()
        observations = self.observations_cache["data"]

        origin = {
            "name": "Riigi Ilmateenistusele",
            "url": "www.ilmateenistus.ee",
            "language": "et-ee",
        }

        min_distance = 10 ** 100
        closest_station = None
        for station in observations["station"]:
            distance = (x - station["point"].x) ** 2 + (y - station["point"].y) ** 2
            if distance < min_distance:
                closest_station = station
                min_distance = distance

        valid_from = datetime.fromtimestamp(int(observations["@timestamp"]))
        valid_to = valid_from + timedelta(hours=1)

        location = {
            "name": closest_station["name"],
            "geometry": closest_station["point"].__geo_interface__,
        }

        location["geometry"]["crs"] = {
            "type": "name",
            "properties": {"name": "EPSG:3301"},
        }

        if closest_station.get("wmocode", None) is not None:
            location["id"] = closest_station.get("wmocode")
            location["authority"] = "WMO"
            location["authority_id"] = closest_station.get("wmocode")
        else:
            location["id"] = hash(location["name"])

        icon = self.icons_mapping.get(closest_station["phenomenon"], None)

        if closest_station.get("visibility", None) is not None:
            visibility = float(closest_station["visibility"]) * 1000
        else:
            visibility = None

        if closest_station.get("precipitations", None) is not None:
            precipitation_intensity = float(closest_station["precipitations"])
        else:
            precipitation_intensity = None

        if closest_station.get("airpressure", None) is not None:
            pressure = float(closest_station["airpressure"])
        else:
            pressure = None

        if closest_station.get("relativehumidity", None) is not None:
            humidity = float(closest_station["relativehumidity"]) / 100
        else:
            humidity = None

        if closest_station.get("airtemperature", None) is not None:
            temperature = float(closest_station["airtemperature"])
        else:
            temperature = None

        if closest_station.get("winddirection", None) is not None:
            wind_bearing = float(closest_station["winddirection"])
        else:
            wind_bearing = None

        if closest_station.get("windspeed", None) is not None:
            wind_speed = float(closest_station["windspeed"])
        else:
            wind_speed = None

        if closest_station.get("windspeedmax", None) is not None:
            wind_gust = float(closest_station["windspeedmax"])
        else:
            wind_gust = None

        if closest_station.get("uvindex", None) is not None:
            uv_index = float(closest_station["uvindex"])
        else:
            uv_index = None

        return [
            {
                "id": f'{location["id"]}|{observations["@timestamp"]}',
                "location": location,
                "origin": origin,
                "fetched_at": self.observations_cache["timestamp"],
                "valid_from": valid_from,
                "valid_to": valid_to,
                "sample_type": "observation",
                "interval_type": "hour",
                "computed_at": valid_from,
                "icon": icon,
                "visibility": visibility,
                "precipitation_intensity": precipitation_intensity,
                "pressure": pressure,
                "humidity": humidity,
                "temperature": temperature,
                "wind_bearing": wind_bearing,
                "wind_speed": wind_speed,
                "wind_gust": wind_gust,
                "uv_index": uv_index,
            }
        ]

    def get_alerts_for_xy(self, x, y):
        """Retrieve the alerts

        Arguments:
            x {float} -- Easting
            y {float} -- Northing

        Returns:
            [type] -- [description]
        """
        if datetime.now() > self.alerts_cache["timestamp"] + timedelta(
            seconds=int(config.ILMATEENISTUS_XML_ALERTS_MAX_AGE)
        ):
            self.fetch_alerts()
        alerts = self.alerts_cache["data"]

        return [
            {
                "id": hash(
                    alert["timestamp"] + alert["area_est"] + alert["content_est"]
                ),
                "sender": "Ilmateenistus",
                "issued_at": datetime.fromtimestamp(int(alert["timestamp"])),
                "valid_from": datetime.fromtimestamp(int(alert["timestamp"])),
                "location": {"id": hash(alert["area_est"]), "name": alert["area_est"]},
                "summmary": alert["content_est"],
                "category": "met",
            }
            for alert in alerts
        ]


ilmateenistus_client = IlmateenistusClient()
