import os
import sys

from pathlib import Path, PurePath

from pydantic import BaseSettings

from typing import Dict, Union


class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_ESTONIAN_1997: str = "3301"
    EPSG_SRID_WGS84: str = "3301"

    OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME: str = (
        "addons-ilmateenistus-meteorology-weather"
    )
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME: str = "127.0.0.1"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT: int = 5775
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1

    ILMATEENISTUS_XML_OBSERVATIONS_URL: str = (
        "https://www.ilmateenistus.ee/ilma_andmed/xml/observations.php"
    )
    ILMATEENISTUS_XML_FORECASTS_URL: str = (
        "https://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php"
    )
    ILMATEENISTUS_XML_ALERTS_URL: str = (
        "https://www.ilmateenistus.ee/ilma_andmed/xml/hoiatus.php"
    )
    ILMATEENISTUS_XML_OBSERVATIONS_MAX_AGE: int = 60
    ILMATEENISTUS_XML_FORECASTS_MAX_AGE: int = 60
    ILMATEENISTUS_XML_ALERTS_MAX_AGE: int = 60

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

config = Settings()